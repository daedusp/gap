#!/usr/bin/python
import unittest, sys, os
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from project import reader


class test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        os.chdir(r"../project/")

    def test_no_list_type(self):
        test_list = reader.convert_to_string_from_a_file(26262)
        self.assertEqual(type(test_list) is list, False)

    def test_is_a_list(self):
        test_list = reader.convert_to_string_from_a_file("example_files/iso_8859-1.txt")
        self.assertEqual(type(test_list) is list, True)

    def test_longest_from_txt(self):
        test_list = reader.convert_to_string_from_a_file("example_files/iso_8859-1.txt")
        longest = reader.get_longest_string_from_list(test_list)
        self.assertEqual("MULTIPLICATION", longest)

    def test_longest_from_json(self):
        test_list = reader.convert_to_string_from_a_file("example_files/json_file.json")
        longest = reader.get_longest_string_from_list(test_list)
        self.assertEqual("language", longest)

    def test_longest_from_csv(self):
        test_list = reader.convert_to_string_from_a_file("example_files/addresses.csv")
        longest = reader.get_longest_string_from_list(test_list)
        self.assertEqual("jefferson", longest)

    def test_longest_list(self):
        test_list = ["abc", "abcd", "abcde"]
        longest = reader.get_longest_string_from_list(test_list)
        self.assertEqual("abcde", longest)

    def test_empty_file(self):
        longest = reader.convert_to_string_from_a_file("example_files/empty.txt")
        self.assertEqual([], longest)

    def test_empty_list(self):
        test_list = []
        longest = reader.get_longest_string_from_list(test_list)
        self.assertEqual(None, longest)

    def test_reverse_string(self):
        value = "12345"
        result = reader.reverse_string(value)
        self.assertEqual("54321", result)

    def test_no_reverse_string(self):
        value = "12345"
        result = reader.reverse_string(value)
        self.assertIsNot(result, value)


if __name__ == "__main__":
    unittest.main()
