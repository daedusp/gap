Requirement:
Python 3.6

Using the CLI Navigate to _Reverse letters in the largest word/tests_ directory

Run `python -m unittest` command

It will display the following output:
`Ran 10 tests in 0.004s`
`OK`


Now, navigate to _Reverse letters in the largest word/project_ directory
Run `python main.py -h`

it will display the following output:
`python main.py -i <inputfile>`
`Example: python main.py -i "example_files/json_file.json"`


There are the following example files:

- addresses.csv
- json_file.json
- iso_8859-1.txt
- empty.txt


Running `python main.py -i "example_files/json_file.json"` will display:

The longest word is: language
The reverse word is: egaugnal



