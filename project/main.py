#!/usr/bin/python3
import sys, getopt, os
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from project import reader


def main(argv):
    try:
        opts, args = getopt.getopt(argv, "hi:o:", ["ifile="])
    except getopt.GetoptError:
        print('Run mail.py within project dir')

        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('python main.py -i <inputfile>')
            print('Example: python main.py -i "example_files/json_file.json"')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            reader_call(arg)


def reader_call(inputfile):
    list_result = reader.convert_to_string_from_a_file(inputfile)
    longest_word = reader.get_longest_string_from_list(list_result)
    reverse_word = reader.reverse_string(longest_word)

    print("The longest word is: " + longest_word)
    print("The reverse word is: " + reverse_word)


if __name__ == "__main__":
    main(sys.argv[1:])
