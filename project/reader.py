#!/usr/bin/python
import re


def convert_to_string_from_a_file(file_arg):
    try:
        if type(file_arg) is list or type(file_arg) is str:
            with open(file_arg) as file:
                return re.findall(r'\w+', file.read())
    except:
        print("An exception occurred")


def get_longest_string_from_list(list_x):
    try:
        if type(list_x) is list:
            return max(list_x, key=len)
    except:
        print("An exception occurred" )


def reverse_string(value=None):
    try:
        if type(value) is str:
            return value[::-1]
    except:
        print("An exception occurred")
